package com.chrishopkin.missioncontrol

import android.content.res.Resources
import androidx.annotation.StringRes

class StringProvider(private val resources: Resources) {

    fun getString(@StringRes id: Int, vararg args: Any) = resources.getString(id, *args)
}