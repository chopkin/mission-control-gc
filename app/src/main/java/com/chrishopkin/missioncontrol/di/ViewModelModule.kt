package com.chrishopkin.missioncontrol.di

import com.chrishopkin.missioncontrol.mission.Mission
import com.chrishopkin.missioncontrol.ui.missiondetail.MissionDetailViewModel
import com.chrishopkin.missioncontrol.ui.missionlist.MissionListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        MissionListViewModel(get(), get())
    }

    viewModel { (mission : Mission) ->
        MissionDetailViewModel(
            mission = mission,
            stringProvider = get()
        )
    }
}