package com.chrishopkin.missioncontrol.di

import com.chrishopkin.missioncontrol.StringProvider
import com.chrishopkin.missioncontrol.mission.MissionRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module {

    single {
        MissionRepository(androidContext().resources)
    }

    single {
        StringProvider(androidContext().resources)
    }
}