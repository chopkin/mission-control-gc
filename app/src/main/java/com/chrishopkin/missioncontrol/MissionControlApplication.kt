package com.chrishopkin.missioncontrol

import android.app.Application
import com.chrishopkin.missioncontrol.di.appModule
import com.chrishopkin.missioncontrol.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MissionControlApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@MissionControlApplication)
            modules(appModule, viewModelModule)
        }
    }
}