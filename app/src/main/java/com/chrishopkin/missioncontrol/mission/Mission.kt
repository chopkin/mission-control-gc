package com.chrishopkin.missioncontrol.mission

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Parcelize
@Serializable
data class Mission(
    @SerialName("name")
    val name: String,
    @SerialName("description")
    val description: String,
    @SerialName("points")
    val points: Int,
    @SerialName("type")
    val type: MissionType
) : Parcelable