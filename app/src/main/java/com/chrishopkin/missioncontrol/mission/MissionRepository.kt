package com.chrishopkin.missioncontrol.mission

import android.content.res.Resources
import com.chrishopkin.missioncontrol.R
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class MissionRepository(resources: Resources) {

    val missions: List<Mission> = Json.decodeFromString(resources.readMissionsText())

    private fun Resources.readMissionsText() = openRawResource(R.raw.missions).bufferedReader().use { reader ->
        reader.readText()
    }
}