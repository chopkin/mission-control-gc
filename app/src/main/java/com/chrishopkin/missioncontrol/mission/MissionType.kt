package com.chrishopkin.missioncontrol.mission

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class MissionType {
    @SerialName("photo")
    PHOTO,
    @SerialName("text")
    TEXT,
    @SerialName("gps")
    GPS
}