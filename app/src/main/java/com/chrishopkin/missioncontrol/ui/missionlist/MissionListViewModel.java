package com.chrishopkin.missioncontrol.ui.missionlist;

import androidx.lifecycle.ViewModel;

import com.chrishopkin.missioncontrol.StringProvider;
import com.chrishopkin.missioncontrol.mission.Mission;
import com.chrishopkin.missioncontrol.mission.MissionRepository;

import java.util.ArrayList;
import java.util.List;

public class MissionListViewModel extends ViewModel {

    private final MissionRepository mMissionRepository;
    private final StringProvider mStringProvider;

    private OnMissionClickListener mMissionClickListener = null;

    public MissionListViewModel(MissionRepository missionRepository, StringProvider stringProvider) {
        mMissionRepository = missionRepository;
        mStringProvider = stringProvider;
    }

    public void setOnMissionClickListener(OnMissionClickListener missionClickListener) {
        mMissionClickListener = missionClickListener;
    }

    public List<MissionViewModel> getMissionViewModels() {
        ArrayList<MissionViewModel> missionViewModels = new ArrayList<>();

        for (Mission mission : mMissionRepository.getMissions()) {
            missionViewModels.add(new MissionViewModel(mission, mStringProvider, this::onMissionClick));
        }

        return missionViewModels;
    }

    private void onMissionClick(Mission mission) {
        if (mMissionClickListener != null) {
            mMissionClickListener.onMissionClick(mission);
        }
    }
}
