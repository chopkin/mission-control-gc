package com.chrishopkin.missioncontrol.ui.missionlist;

import com.chrishopkin.missioncontrol.mission.Mission;

interface OnMissionClickListener {
    void onMissionClick(Mission mission);
}
