package com.chrishopkin.missioncontrol.ui.missionlist;

import static androidx.recyclerview.widget.RecyclerView.HORIZONTAL;
import static org.koin.androidx.viewmodel.compat.ViewModelCompat.viewModel;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chrishopkin.missioncontrol.R;
import com.chrishopkin.missioncontrol.databinding.FragmentMissionListBinding;
import com.chrishopkin.missioncontrol.mission.Mission;

import kotlin.Lazy;


public class MissionListFragment extends Fragment {

    private final Lazy<MissionListViewModel> mViewModel = viewModel(this, MissionListViewModel.class);

    public MissionListFragment() {
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState
    ) {
        FragmentMissionListBinding binding =
                FragmentMissionListBinding.inflate(inflater, container, false);

        mViewModel.getValue().setOnMissionClickListener(this::onMissionClick);

        setListDivider(binding.missionList);

        binding.missionList.setAdapter(new MissionListAdapter(mViewModel.getValue().getMissionViewModels()));

        return binding.getRoot();
    }

    private void setListDivider(RecyclerView list) {
        LinearLayoutManager layoutManager = (LinearLayoutManager) list.getLayoutManager();

        int orientation = HORIZONTAL;

        if (layoutManager != null) {
            orientation = layoutManager.getOrientation();
        }

        Drawable dividerDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.divider, null);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(list.getContext(), orientation);

        if (dividerDrawable != null){
            dividerItemDecoration.setDrawable(dividerDrawable);
        }

        list.addItemDecoration(dividerItemDecoration);
    }

    private void onMissionClick(Mission mission) {
        NavHostFragment.findNavController(this)
                .navigate(MissionListFragmentDirections.actionShowMissionDetail(mission));
    }
}