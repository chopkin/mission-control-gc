package com.chrishopkin.missioncontrol.ui.missionlist;

import androidx.annotation.DrawableRes;

import com.chrishopkin.missioncontrol.R;
import com.chrishopkin.missioncontrol.StringProvider;
import com.chrishopkin.missioncontrol.mission.Mission;

public class MissionViewModel {

    private final Mission mMission;
    private final StringProvider mStringProvider;
    private final OnMissionClickListener mMissionClickListener;

    public MissionViewModel(
            Mission mission,
            StringProvider stringProvider,
            OnMissionClickListener missionClickListener
    ) {
        mMission = mission;
        mStringProvider = stringProvider;
        mMissionClickListener = missionClickListener;
    }

    public String getName() {
        return mMission.getName();
    }

    public String getDescription() {
        return mMission.getDescription();
    }

    public String getPoints() {
        int points = mMission.getPoints();
        return mStringProvider.getString(R.string.points, points);
    }

    @DrawableRes
    public int getTypeDrawable() {
        int drawable = R.drawable.ic_photo;

        switch (mMission.getType()) {
            case PHOTO:
                drawable = R.drawable.ic_photo;
                break;
            case TEXT:
                drawable = R.drawable.ic_text;
                break;
            case GPS:
                drawable = R.drawable.ic_gps;
                break;
        }

        return drawable;
    }

    public void onClick() {
        mMissionClickListener.onMissionClick(mMission);
    }
}
