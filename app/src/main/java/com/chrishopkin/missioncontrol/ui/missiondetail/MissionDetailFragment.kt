package com.chrishopkin.missioncontrol.ui.missiondetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.chrishopkin.missioncontrol.databinding.FragmentMissionDetailBinding
import com.chrishopkin.missioncontrol.ui.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MissionDetailFragment : Fragment() {

    private val safeArgs
        get() = MissionDetailFragmentArgs.fromBundle(requireArguments())

    private val viewModel: MissionDetailViewModel by viewModel { parametersOf(safeArgs.mission) }

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainViewModel.setTitle(viewModel.name)

        return FragmentMissionDetailBinding.inflate(inflater, container, false).also { binding ->
            binding.missionItem.apply {
                missionName.text = viewModel.name
                missionDescription.text = viewModel.description
                missionPoints.text = viewModel.points
                missionImage.setImageResource(viewModel.typeDrawable)
            }

            binding.missionButton.setOnClickListener {
                viewModel.onMissionButtonClick()
            }
        }.root
    }
}