package com.chrishopkin.missioncontrol.ui.missionlist;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chrishopkin.missioncontrol.databinding.LayoutMissionItemBinding;

class MissionListViewHolder extends RecyclerView.ViewHolder {

    private final LayoutMissionItemBinding mBinding;

    public MissionListViewHolder(@NonNull LayoutMissionItemBinding binding) {
        super(binding.getRoot());

        mBinding = binding;
    }

    public void bind(MissionViewModel viewModel) {
        mBinding.missionImage.setImageResource(viewModel.getTypeDrawable());
        mBinding.missionName.setText(viewModel.getName());
        mBinding.missionDescription.setText(viewModel.getDescription());
        mBinding.missionPoints.setText(viewModel.getPoints());

        mBinding.getRoot().setOnClickListener(v -> viewModel.onClick());
    }
}
