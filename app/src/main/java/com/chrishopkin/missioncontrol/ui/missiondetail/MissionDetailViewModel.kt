package com.chrishopkin.missioncontrol.ui.missiondetail

import android.util.Log
import androidx.lifecycle.ViewModel
import com.chrishopkin.missioncontrol.R
import com.chrishopkin.missioncontrol.StringProvider
import com.chrishopkin.missioncontrol.mission.Mission
import com.chrishopkin.missioncontrol.mission.MissionType

class MissionDetailViewModel(mission: Mission, stringProvider: StringProvider) : ViewModel() {

    private companion object {
        const val LOG_TAG = "MissionDetailViewModel"
    }

    val name = mission.name

    val description = mission.description

    val points = stringProvider.getString(R.string.points, mission.points)

    val typeDrawable = when (mission.type) {
        MissionType.PHOTO -> R.drawable.ic_photo
        MissionType.TEXT -> R.drawable.ic_text
        MissionType.GPS -> R.drawable.ic_gps
    }

    fun onMissionButtonClick() {
        Log.d(LOG_TAG, "Mission button clicked for mission '$name'")
    }
}