package com.chrishopkin.missioncontrol.ui.missionlist;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chrishopkin.missioncontrol.databinding.LayoutMissionItemBinding;

import java.util.List;

public class MissionListAdapter extends RecyclerView.Adapter<MissionListViewHolder> {

    private final List<MissionViewModel> mMissionViewModels;

    public MissionListAdapter(List<MissionViewModel> missionViewModels) {
        mMissionViewModels = missionViewModels;
    }

    @NonNull
    @Override
    public MissionListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutMissionItemBinding binding =
                LayoutMissionItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);

        return new MissionListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MissionListViewHolder holder, int position) {
        holder.bind(mMissionViewModels.get(position));
    }

    @Override
    public int getItemCount() {
        return mMissionViewModels.size();
    }
}
